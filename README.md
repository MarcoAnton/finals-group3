# course and section;
BS Computer Science CSCI 40-E

# group members’  full name, ID number;
# full name format → last name, first name, M.I.
# in alphabetical order by last name
Amador, John Michael T. 206105
Andrada, Alvin Joshua M. 195478
Fausto, Brendan Gabrielle M. 202033
Paeldon, Marco Anton O. 193752
Rodillas, Ron Lawrence C. 204364

# project title;
Widget v2

# members’ app assignments;
Amador - Assignments App
Andrada - Calendar App
Fausto - Dashboard App
Paeldon- Announcements App
Rodillas - Forum App
# date of submission;
15 May 2023

# a statement, in your group’s own words, that the project was truthfully completed by your group;
This project was accomplished truthfully only by the people whose names are listed above.

# list of references used;

 Datetime Import
 https://docs.python.org/3/library/datetime.html
 
 Button as Link
 https://stackoverflow.com/questions/2906582/how-do-i-create-an-html-button-that-acts-like-a-link


# members’ signatures in the form (sgd) your complete name, date
Alvin Joshua M. Andrada (sgd) May 14, 2023
John Michael T. Amador (sgd) May 9, 2023
Brendan Gabrielle M. Fausto (sgd) May 15, 2023
Marco Anton O. Paeldon (sgd) May 14, 2023
Ron Lawrence C. Rodillas (sgd) May 14, 2023
