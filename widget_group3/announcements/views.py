from django.shortcuts import render
from django.http import HttpResponse
from .models import Announcement, Reaction
import datetime
from django.views.generic import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.urls import reverse

def index(request):
    overall_board = "<html><title>Announcement Board</title><body>" \
                  "<h1> Widget's Announcement Board</h1>"
    overall_board += "Announcements:"
    for announcement in Announcement.objects.all():
        overall_board += "<br>%s by " %announcement.title
        overall_board += "%s " %announcement.author.first_name
        overall_board += "%s published " %announcement.author.last_name
        overall_board += "%s, " %announcement.pub_datetime.date().strftime("%m/%d/%Y")
        overall_board += "%s<br>" %announcement.pub_datetime.time().strftime("%I:%M %p")
        overall_board += "%s<br>" %announcement.body
        for reaction in Reaction.objects.all():
            if reaction.announcement == announcement:
                overall_board += "%s: " %reaction.name
                overall_board += "%i<br>" %reaction.tally

    overall_board += "</body></html>"
    return HttpResponse(overall_board)


def announcements_view(request):
    announcements = Announcement.objects.all().order_by('-pub_datetime')
    return render(request, 'announcements/announcements.html', {'announcements': announcements})


class AnnouncementsDetailView(DetailView):
    def get(self, request, pk):
        myannouncement = Announcement.objects.get(pk=pk)
        reactions = myannouncement.reactions.all().values()
        context = {
            'myannouncement': myannouncement,
            'reactions': reactions,
        }
        return render(request, 'announcements/announcement-details.html', context)



class AnnouncementsCreateView(CreateView):
    model = Announcement
    fields = ["title", "body", "author"]
    template_name = 'announcements/announcement-add.html'

    def get_success_url(self):
       return reverse('announcements:announcements-details', kwargs={'pk': self.object.pk})



class AnnouncementsUpdateView(UpdateView):
    model = Announcement
    fields = ["title", "body", "author"]
    template_name = 'announcements/announcement-edit.html'

    def get_success_url(self):
       return reverse('announcements:announcements-details', kwargs={'pk': self.object.pk})

