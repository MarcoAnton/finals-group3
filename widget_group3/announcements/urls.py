from django.urls import path

from .views import announcements_view, AnnouncementsDetailView, AnnouncementsCreateView, AnnouncementsUpdateView

urlpatterns = [
    path('', announcements_view, name='announcements_page'),
    path('<int:pk>/details/', AnnouncementsDetailView.as_view(), name='announcements-details'),
    path('add', AnnouncementsCreateView.as_view(), name='announcements-add'),
    path('<int:pk>/details/edit', AnnouncementsUpdateView.as_view(), name='announcements-edit'),
]


app_name = "announcements"