# Generated by Django 4.2.1 on 2023-05-14 10:26

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('announcements', '0010_alter_announcement_pub_datetime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='announcement',
            name='pub_datetime',
            field=models.DateTimeField(null=True, verbose_name=datetime.datetime(2023, 5, 14, 10, 26, 29, 923612, tzinfo=datetime.timezone.utc)),
        ),
    ]
