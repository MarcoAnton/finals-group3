from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse
import django

class Announcement(models.Model):
    title = models.CharField(max_length=255, null=True)
    body = models.TextField(null=True)
    author = models.ForeignKey('dashboard.WidgetUser', on_delete=models.CASCADE, null=True)
    pub_datetime = models.DateTimeField(default = django.utils.timezone.now, null=True)

    def __str__(self):
        return f"{self.title}"


class Reaction(models.Model):
    name = models.CharField(max_length=255, null=True)
    tally = models.IntegerField(null=True)
    announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE, null=True, related_name='reactions')

    def __str__(self):
        return f"{self.name}"