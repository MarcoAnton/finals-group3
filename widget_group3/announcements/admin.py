from django.contrib import admin

from .models import Announcement, Reaction


class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement

    list_display = ('title', 'body', 'author',)



class ReactionAdmin(admin.ModelAdmin):
    model = Reaction

    list_display = ('name', 'tally', 'announcement',)


admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)