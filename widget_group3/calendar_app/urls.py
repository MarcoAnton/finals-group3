#calendar_app/urls

from django.urls import path

from .views import event_view, EventDetailView, EventCreateView, EventUpdateView

urlpatterns = [
    path('', event_view, name='event_view'),
    path('events/<int:pk>/details/', EventDetailView.as_view(), name='event-details'),
    path('events/add', EventCreateView.as_view(), name='event-add'),
    path('events/<int:pk>/edit', EventUpdateView.as_view(), name='event-edit'),
]


app_name = "calendar_app"
