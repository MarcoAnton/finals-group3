from django.contrib import admin

from .models import Event, Location

class EventInline(admin.TabularInline):
    model = Event

class EventAdmin(admin.ModelAdmin):
    model = Event

class LocationAdmin(admin.ModelAdmin):
    model = Location
    
    #Shows all events that occur at the same location
    inlines = [EventInline,]

admin.site.register(Event, EventAdmin)
admin.site.register(Location, LocationAdmin)

