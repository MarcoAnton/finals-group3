from django.db import models
from Assignments.models import Course 
from django.urls import reverse

class Event(models.Model):
    activity = models.CharField(max_length=100)
    target_datetime = models.DateTimeField()
    estimated_hours = models.FloatField()
    location = models.ForeignKey(
        'Location',
        on_delete = models.CASCADE,
    )   
    course = models.ForeignKey(
        'Assignments.Course',
        on_delete = models.CASCADE,
    )
    
    def __str__(self):
        return '{} on {}'.format(self.activity, self.target_datetime)

    
class Location(models.Model):
    mode_choices = [
        ('onsite','onsite'),
        ('online','online'),
        ('hybrid','hybrid'),    
    ]
    mode = models.CharField(
        max_length=6,
        choices=mode_choices,
        default='onsite',        
    )
    venue = models.CharField(max_length=200)
    
    def __str__(self):
        return '{} - {}'.format(self.mode, self.venue)

