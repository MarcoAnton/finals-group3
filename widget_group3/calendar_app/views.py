#References: 
 # Queries - Django Documentation
 # https://docs.djangoproject.com/en/4.1/topics/db/queries/#retrieving-a-single-object-with-get

 # Datetime Import
 # https://docs.python.org/3/library/datetime.html

from django.shortcuts import render
from django.http import HttpResponse
from .models import Event, Location
from django.views.generic import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.urls import reverse
import datetime
 
def event_view(request):
    event = Event.objects.all()
    return render(request, 'calendar_app/calendar.html', {'event': event})

class EventDetailView(DetailView):
    model = Event
    template_name = 'calendar_app/event-details.html'

class EventCreateView(CreateView):
    model = Event
    fields = '__all__'
    template_name = 'calendar_app/event-add.html'
    
    #after creating form, sends user to detail page of created activity
    def get_success_url(self):
       return reverse('calendar_app:event-details', kwargs={'pk': self.object.pk})

class EventUpdateView(UpdateView):
    model = Event
    fields = '__all__'
    template_name = 'calendar_app/event-edit.html'
    
    #after updating form, sends user to detail page of created activity
    def get_success_url(self):
       return reverse('calendar_app:event-details', kwargs={'pk': self.object.pk})
