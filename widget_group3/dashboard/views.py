from django.shortcuts import render, redirect
from django.views import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.http import HttpResponse

from .models import WidgetUser

# Create your views here.


def dashboard(request):
    users = WidgetUser.objects.all()
    return render(request, 'dashboard/dashboard.html', {
        'users': users
    })

        
class WidgetUserCreateView(CreateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-add.html'


class WidgetUserUpdateView(UpdateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-edit.html'


class WidgetUserDetailView(DetailView):
    model = WidgetUser
    template_name = 'dashboard/widgetuser-details.html'
    
    def get(self, request, *args, **kwargs):
        print("ActivityDetailView is being called")
        return super().get(request, *args, **kwargs)
    
