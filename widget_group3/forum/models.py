from django.db import models
from django.urls import reverse

# Create your models here.
class ForumPost (models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    author = models.ForeignKey('dashboard.WidgetUser', on_delete=models.CASCADE, null=True)
    pub_datetime = models.DateTimeField(auto_now_add=True)
   
    
    def __str__(self):
        return self.title
        
    def datetime_posted(self):
        return self.pub_datetime.strftime('%m/%d/%Y, %I:%M %p')
        
    def get_absolute_url(self):
        return reverse('forum:PostDetailView', kwargs={'pk': self.pk})
        
    def get_update_url(self):
        return reverse('forum:PostUpdateView', kwargs={'pk': self.pk})
        
        
class Reply (models.Model):
    body = models.TextField()
    author = models.ForeignKey('dashboard.WidgetUser', on_delete=models.CASCADE, null=True)
    pub_datetime = models.DateTimeField(auto_now_add=True)
    forum_post = models.ForeignKey(ForumPost, on_delete=models.CASCADE, related_name = 'post_replies')
    
    def datetime_posted(self):
        return self.pub_datetime.strftime('%m/%d/%Y, %I:%M %p')
        
     