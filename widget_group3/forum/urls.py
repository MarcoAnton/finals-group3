from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('forumposts/add/', PostCreateView.as_view(), name='addPost'),
    path('forumposts/<int:pk>/details', PostDetailView.as_view(), name='PostDetailView'),
    path('forumposts/<int:pk>/edit', PostUpdateView.as_view(), name='PostUpdateView'),
]

app_name="forum"