from django.contrib import admin
from .models import ForumPost, Reply

# Register your models here.
class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost 
    list_display = ('title', 'author', 'pub_datetime')
    search_fields = ('title', 'author')
    list_filter = ('pub_datetime',)
    
class ReplyAdmin(admin.ModelAdmin):
    model = Reply
    list_display = ('author', 'pub_datetime')
    search_fields = ('author',)
    list_filter = ('pub_datetime',)
    
admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)