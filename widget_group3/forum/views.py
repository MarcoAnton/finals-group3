from django.shortcuts import render
from django.http import HttpResponse
from .models import ForumPost, Reply
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView

# Create your views here.
def index(request):
    post_list = reversed(ForumPost.objects.all())
    return render(request,"forum/forum.html",{"posts":post_list})
    
class PostCreateView(CreateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-add.html'

class PostUpdateView(UpdateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-edit.html'
    
class PostDetailView(DetailView):
    model = ForumPost
    template_name = 'forum/forumpost-details.html'
    

