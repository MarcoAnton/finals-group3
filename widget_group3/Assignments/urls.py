from django.urls import path
from .views import assignments_view, AssignmentDetailView, AssignmentCreateView, AssignmentUpdateView

urlpatterns = [
    path('', assignments_view, name='assignments_page'),
    path('<int:pk>/details/', AssignmentDetailView.as_view(), name='assignment-details'),
    path('add', AssignmentCreateView.as_view(), name='assignment-add'),
    path('<int:pk>/details/edit', AssignmentUpdateView.as_view(), name='assignment-edit'),
]
# This might be needed, depending on your Django version
app_name = "Assignments"
