from django.db import models
from django.urls import reverse


# Create your models here.
class Course(models.Model):
    code = models.CharField(max_length=10)
    title = models.CharField(max_length=10000)
    section = models.CharField(max_length=3)

    def __str__(self):
        return '{}-{}'.format(self.code, self.section)

    def get_absolute_url(self):
        return reverse('course_detail', args=[str(self.section)])


class Assignment(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=10000)
    course = models.CharField(max_length=10000)
    perfect_score = models.IntegerField()
    course_stuff = models.ForeignKey(Course, on_delete=models.CASCADE)

    def passing_score(self):
        return '{}'.format(int(self.perfect_score*0.6))

    def __str__(self):
        return '{}: {}'.format(self.name, self.course)

    def get_absolute_url(self):
        return reverse('Assignments:assignment-details', kwargs={'pk': self.pk})
