from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import CreateView, UpdateView
from django.views.generic.detail import DetailView
from .models import Assignment

# Create your view here.


def index(request):
    overall_assignments = "<html><title>Assignments</title><body>" \
                  "<h1> Widget's Assignments Page!</h1>"
    for assignment in Assignment.objects.all():
        overall_assignments += "<p><b>Assignment Name: %s</b><br>" %assignment.name
        overall_assignments += "Description: %s<br>" %assignment.description
        overall_assignments += "Perfect Score: %i<br>" %assignment.perfect_score
        perfect_s = float(assignment.perfect_score)
        overall_assignments += "Passing Score: %i<br>" %(float(perfect_s)*0.6)
        overall_assignments += "Course/Section: %s" %assignment.course_stuff

    overall_assignments += "</body></html>"
    return HttpResponse(overall_assignments)


def assignments_view(request):
    assignment = Assignment.objects.all()
    return render(request, 'Assignments/assignments.html', {'assignment': assignment})


class AssignmentDetailView(DetailView):
    model = Assignment
    template_name = 'Assignments/assignment-details.html'


class AssignmentCreateView(CreateView):
    model = Assignment
    fields = '__all__'
    template_name = 'Assignments/assignment-add.html'

class AssignmentUpdateView(UpdateView):
    # specify the model you want to use
    model = Assignment
    fields = '__all__'
    # specify the fields

    # can specify success url
    # url to redirect after successfully
    # updating details
    template_name = 'Assignments/assignment-edit.html'
